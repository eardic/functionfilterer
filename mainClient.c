/* CLIENT DRIVER
 *
 * Created BY: EMRE ARDIÇ
 * Creation DATE : 25/05/2012
 *
 * Description :
 *
 */


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include "client.h"
#include "types.h"
#include "server.h"

void GetArguments(int argc, LPTSTR argv[], DOUBLEPTR cutoff, DOUBLEPTR damping, LPTSTR server, INTPTR port);
void Usage(LPTSTR progName);

int main(int argc, LPTSTR argv[]) {
    double cutoff, damping;
    char server[MAXHOSTNAME];
    int port;
    GetArguments(argc, argv, &cutoff, &damping, server, &port);
    StartClient(cutoff, damping, server, port);
    return EXIT_SUCCESS;
}

/* Returns tree_depth */
void GetArguments(int argc, LPTSTR argv[], DOUBLEPTR cutoff, DOUBLEPTR damping, LPTSTR server, INTPTR port) {
    if (argc < 2) {
        fprintf(stderr, "\nError :Missing argument.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

    if (!strcmp(argv[1], "-h")) {
        Usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    if (argv[1] != NULL && !strcmp(argv[1], "-cutoff")) {
        if (argv[2] == NULL) {
            fprintf(stderr, "\nError :Invalid cutoff.\nTry -h.\n\n");
            exit(EXIT_FAILURE);
        }
        sscanf(argv[2], "%lf", cutoff);
    } else {
        fprintf(stderr, "\nError :Cutoff option error.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

    if (argv[3] != NULL && !strcmp(argv[3], "-damping")) {
        if (argv[4] == NULL) {
            fprintf(stderr, "\nError :Invalid damping.\nTry -h.\n\n");
            exit(EXIT_FAILURE);
        }
        sscanf(argv[4], "%lf", damping);
    } else {
        fprintf(stderr, "\nError :Damping option error.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

    if (argv[5] != NULL && !strcmp(argv[5], "-server")) {
        if (argv[6] == NULL) {
            fprintf(stderr, "\nError :Invalid server.\nTry -h.\n\n");
            exit(EXIT_FAILURE);
        }
        strcpy(server, argv[6]);
    } else {
        fprintf(stderr, "\nError :Server option error.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

    if (argv[7] != NULL && !strcmp(argv[7], "-port")) {
        if (argv[8] == NULL) {
            fprintf(stderr, "\nError :Invalid port.\nTry -h.\n\n");
            exit(EXIT_FAILURE);
        }
        *port = atoi(argv[8]);
    } else {
        fprintf(stderr, "\nError :Port option error.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

}

/* Prints usage to screen */
void Usage(LPTSTR progName) {
    printf("\nThe Client program receiving function data from server.");
    printf("\nOption : -h for usage.");
    printf("\nUsage : %s -cutoff [#] -damping [#] -server [#] -port [#]\n\n", progName);
}

