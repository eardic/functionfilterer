#############################################
#  	Makefile for Server and Team 	    #
#############################################
# EMRE ARDIÇ         #   SYSTEM PROGRAMMING #
# 091044056 FINAL    # 			    #
#############################################

CC=gcc
CFLAGS=-c
LIBS=-lrt -pthread

all : server client logger

client : mainClient.o client.o
	$(CC) mainClient.o client.o -o client $(LIBS)

server : mainServer.o server.o
	$(CC) mainServer.o server.o -o server $(LIBS)

logger : mainLogger.o logger.o
	$(CC) mainLogger.o logger.o -o logger $(LIBS)

mainServer.o : mainServer.c
	$(CC) $(CFLAGS) mainServer.c 

mainClient.o : mainClient.c
	$(CC) $(CFLAGS) mainClient.c 

client.o : client.c client.h
	$(CC) $(CFLAGS) client.c

server.o : server.c server.h
	$(CC) $(CFLAGS) server.c

logger.o : logger.c logger.h 
	$(CC) $(CFLAGS) logger.c

clean :
	rm *.o