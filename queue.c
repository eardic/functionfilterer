#include<stdio.h>
#include<stdlib.h>
#include "queue.h"

/* Maximum element number and size of an element must be given, returns queue ptr */
QUEUEPTR Create(int maxElements)
{
    /* Create a Queue */
    QUEUEPTR Q;
    Q = (QUEUEPTR) malloc(sizeof (Queue));
    /* Initialise its properties */
    Q->elements = malloc(sizeof (VOIDPTR) * maxElements);
    Q->size = 0;
    Q->capacity = maxElements;
    Q->front = 0;
    Q->rear = -1;
    /* Return the pointer */
    return Q;
}

VOIDPTR Pop(QUEUEPTR Q)
{
    /* If Queue size is zero then it is empty. So we cannot pop */
    if (Q->size == 0) {
        printf("Queue is Empty\n");
        return NULL;
    }
        /* Removing an element is equivalent to incrementing index of front by one */
    else {
        Q->size--;
        Q->front++;
        /* As we fill elements in circular fashion */
        if (Q->front == Q->capacity) {
            Q->front = 0;
        }
        return Q->elements[Q->front - 1];
    }
}

void Destroy(QUEUEPTR Q)
{
    free(Q->elements);
    free(Q);
}

VOIDPTR Peek(QUEUEPTR Q)
{
    if (Q->size == 0) {
        printf("Queue is Empty\n");
        exit(0);
    }
    /* Return the element which is at the front*/
    return Q->elements[Q->front];
}

int Push(QUEUEPTR Q, VOIDPTR element)
{
    /* If the Queue is full, we cannot push an element into it as there is no space for it.*/
    if (Q->size == Q->capacity) {
        printf("Queue is Full\n");
        return FAIL;
    }
    else {
        Q->size++;
        Q->rear = Q->rear + 1;
        /* As we fill the queue in circular fashion */
        if (Q->rear == Q->capacity) {
            Q->rear = 0;
        }
        /* Insert the element in its rear side */
        Q->elements[Q->rear] = element;
    }
    return SUCCESS;
}
