/* CLIENT IMPLEMENTATION FILE
 *
 * Created BY: EMRE ARDIÇ
 * Creation DATE : 25/05/2012
 *
 * Description :
 *
 */

#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h> /* nanosecond */
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "client.h"
#include "types.h"
#include <float.h>

/* Connects server using the key */
int ConnectServer(LPTSTR hostname, unsigned short portNum) {
    struct sockaddr_in sa;
    struct hostent *hp;
    int s;

    if ((hp = gethostbyname(hostname)) == NULL) {
        errno = ECONNREFUSED;
        perror("Failed to get hostname");
        return FAIL;
    }

    memset(&sa, 0, sizeof (sa));
    memcpy((char *) &sa.sin_addr, hp->h_addr, hp->h_length);

    sa.sin_family = hp->h_addrtype;
    sa.sin_port = htons((u_short) portNum);

    if ((s = socket(hp->h_addrtype, SOCK_STREAM, 0)) == FAIL) {
        perror("Failed to get socket");
        return FAIL;
    }

    if (connect(s, (struct sockaddr *) &sa, sizeof sa) == FAIL) {
        perror("Failed connect socket");
        close(s);
        return FAIL;
    }

    return (s);
}

int ReceiveMessage(int socket, VOIDPTR message, int size) {
    LPTSTR data = (LPTSTR) message;
    int i,
            ret;

    for (i = 0; i < size; ++i) {
        if ((ret = read(socket, data + i, 1)) == FAIL) {
            perror("Failed to get data from socket");
            return FAIL;
        }
    }

    return SUCCESS;
}

int SendMessage(int socket, VOIDPTR message, int size) {
    LPTSTR data = (LPTSTR) message;
    int i,
            ret;

    for (i = 0; i < size; ++i) {
        if ((ret = write(socket, data + i, 1)) == FAIL) {
            perror("Failed to send data to socket");
            return FAIL;
        }
    }

    return SUCCESS;
}

/* Starts client program */
int StartClient(double cutoff, double damping, LPTSTR server, int port) {
    int socketFd,
            waitRet;
    char data[20];

    //for (;;) {
        if ((socketFd = ConnectServer(server, port)) == FAIL) {
            fprintf(stderr, "\nError : Failed to connect server.\n");
            return FAIL;
        }
        ReceiveMessage(socketFd, data, 20);
        printf("Data Received : %s", data);
        close(socketFd);
   // }

    return SUCCESS;
}

/* Return time in nano sec. */
double GetNanoSecondTime() {
    struct timespec time;

    if (clock_gettime(CLOCK_REALTIME, &time) == FAIL) {
        perror("Failed to get time");
        return FAIL;
    }

    return time.tv_sec + (double) time.tv_nsec / (double) BILLION;
}


