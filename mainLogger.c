/* SERVER DRIVER
 *
 * Created BY: EMRE ARDIÇ
 * Creation DATE : 25/05/2012
 *
 * Description :
 *
 */

#include "logger.h"
#include "types.h"
#include <stdlib.h>
#include <string.h>

void GetArguments(int argc, LPTSTR argv[], INTPTR maxWorker, LPTSTR server, INTPTR port);

void Usage(LPTSTR progName);

int main(int argc, LPTSTR argv[])
{
    int port, maxWorker;
    char clientHostname[MAXHOSTNAME];
    GetArguments(argc, argv, &maxWorker, clientHostname, &port);
    StartLogger(maxWorker, clientHostname, port);
    return EXIT_SUCCESS;
}

/* Returns tree_depth */
void GetArguments(int argc, LPTSTR argv[], INTPTR maxWorker, LPTSTR server, INTPTR port)
{
    if (argc < 2) {
        fprintf(stderr, "\nError :Missing argument.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

    if (!strcmp(argv[1], "-h")) {
        Usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    if (argv[1] != NULL && !strcmp(argv[1], "-maxWorker")) {
        if (argv[2] == NULL) {
            fprintf(stderr, "\nError :Invalid max worker.\nTry -h.\n\n");
            exit(EXIT_FAILURE);
        }
        *maxWorker = atoi(argv[2]);
    }
    else {
        fprintf(stderr, "\nError :Shared memory option error.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

    if (argv[3] != NULL && !strcmp(argv[3], "-client")) {
        if (argv[4] == NULL) {
            fprintf(stderr, "\nError :Invalid client address.\nTry -h.\n\n");
            exit(EXIT_FAILURE);
        }
        strcpy(server, argv[4]);
    }
    else {
        fprintf(stderr, "\nError :Client address option error.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

    if (argv[5] != NULL && !strcmp(argv[5], "-port")) {
        if (argv[6] == NULL) {
            fprintf(stderr, "\nError :Invalid client port.\nTry -h.\n\n");
            exit(EXIT_FAILURE);
        }
        *port = atoi(argv[6]);
    }
    else {
        fprintf(stderr, "\nError :Port option error.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

}

/* Prints usage to screen */
void Usage(LPTSTR progName)
{
    printf("\nThe Logger program writing function data to corresponding log files.");
    printf("\nOption : -h for usage.");
    printf("\nUsage : %s -maxWorker [#] -client [HOSTNAME] -port [#]\n\n", progName);
}
