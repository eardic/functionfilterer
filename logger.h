/* SERVER HEADER
 *
 * Created BY: EMRE ARDIÇ
 * Creation DATE : 6/06/2012
 *
 * Description :
 *
 * Prototypes of server functions.
 * FAIL,SUCCESS defines.
 *
 */

#ifndef SERVER_H
#define SERVER_H

#include "types.h"

#define SUCCESS 0
#define FAIL -1
#define FALSE 0
#define TRUE 1
#define MAXHOSTNAME 30
#define MAXCONNECTION 10

void StartLogger(const int maxWorker, LPTSTR clientHostname, int port);
int ReceiveMessage(int socket, VOIDPTR message, int size);
int SendMessage(int socket, VOIDPTR message, int size);
int CreateSocket(unsigned short portNum, int maxConnection);
int GetConnection(int s);
VOIDPTR ClientHandler(VOIDPTR arg);

#endif
