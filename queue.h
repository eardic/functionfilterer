/* 
 * File:   queue.h
 * Author: viplime
 *
 * Created on 19 June 2014, 18:53
 */

#ifndef QUEUE_H
#define	QUEUE_H

#include "types.h"

#define SUCCESS 0
#define FAIL -1

/*Queue has five properties. capacity stands for the maximum number of elements Queue can hold.
  Size stands for the current size of the Queue and elements is the array of elements. front is the
 index of first element (the index at which we remove the element) and rear is the index of last element
 (the index at which we insert the element) */
typedef struct Queue {
    int capacity;
    int size;
    int front;
    int rear;
    VOIDPTR *elements;
} Queue;
typedef Queue* QUEUEPTR;

QUEUEPTR Create(int maxElements);
void Destroy(QUEUEPTR Q);
VOIDPTR Pop(QUEUEPTR Q);
VOIDPTR Peek(QUEUEPTR Q);
int Push(QUEUEPTR Q, VOIDPTR element);

#endif	/* QUEUE_H */

