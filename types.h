/* TYPE DEFINITIONS
 *
 * Created BY: EMRE ARDIÇ
 * Creation DATE : 23/04/2012
 *
 * Description :
 *
 * The type definitions by using typedef
 *
 */



#ifndef TYPES_H
#define TYPES_H

#include <stdio.h>
#include <pthread.h>
#include <dirent.h> /* DIR* */

typedef pthread_t * THREADPTR;
typedef void (*Handler)(int);
typedef long * LONGPTR;
typedef int * INTPTR;
typedef double* DOUBLEPTR;
typedef void * VOIDPTR;
typedef char * LPTSTR;
typedef const char * LPTCSTR;

#endif
