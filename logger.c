/* SERVER IMPLEMENTATION FILE
 *
 * Created BY: EMRE ARDIÇ
 * Creation DATE : 6/06/2012
 *
 * Description :
 *
 */

#include "logger.h"
#include "server.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h> /* memset */
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <semaphore.h>

sem_t clientSem;

/* Code to establish a socket; originally from bzs@bu-cs.bu.edu */
int CreateSocket(unsigned short portNum, int maxConnection)
{
    char myname[MAXHOSTNAME + 1];
    int socketFd;
    struct sockaddr_in sa;
    struct hostent *hp;

    memset(&sa, 0, sizeof (struct sockaddr_in));

    gethostname(myname, MAXHOSTNAME);

    hp = gethostbyname(myname);

    if (hp == NULL) {
        return (FAIL);
    }

    sa.sin_family = hp->h_addrtype;
    sa.sin_port = htons(portNum);

    if ((socketFd = socket(AF_INET, SOCK_STREAM, 0)) == FAIL) {
        perror("Failed to create socket");
        return (FAIL);
    }


    if (bind(socketFd, (struct sockaddr *) &sa, sizeof (struct sockaddr_in)) == FAIL) {
        perror("Failed to bind");
        close(socketFd);
        return (FAIL);
    }

    listen(socketFd, maxConnection);

    return (socketFd);
}

int GetConnection(int socketFd)
{
    int retVal;

    if ((retVal = accept(socketFd, NULL, NULL)) == FAIL) {
        perror("Failed to accept");
        return FAIL;
    }

    return retVal;
}

int ReceiveMessage(int socket, VOIDPTR message, int size)
{
    LPTSTR data = (LPTSTR) message;
    int i,
            ret;

    for (i = 0; i < size; ++i) {
        if ((ret = read(socket, data + i, 1)) == FAIL) {
            perror("Failed to get data from socket");
            return FAIL;
        }
    }

    return SUCCESS;
}

int SendMessage(int socket, VOIDPTR message, int size)
{
    int i,
            ret;

    LPTSTR data = (LPTSTR) message;

    for (i = 0; i < size; ++i) {
        if ((ret = write(socket, data + i, 1)) == FAIL) {
            perror("Failed to send data to socket");
            return FAIL;
        }
    }

    return SUCCESS;
}

void StartLogger(const int maxWorker, LPTSTR clientHostname, int port)
{
    int socket;
    int socketFd;
    pthread_t worker;

    sem_init(&clientSem, 0, maxWorker);

    socket = CreateSocket(port, MAXCONNECTION);

    for (;;) {
        if ((socketFd = GetConnection(socket)) == FAIL) {
            fprintf(stderr, "\nError:Failed to accept connection.\n");
            break;
        }

        if (pthread_create(&worker, NULL, ClientHandler, &socketFd)) {
            fprintf(stderr, "\nError: Failed to create connection handler\n");
            return;
        }
    }

    sem_destroy(&clientSem);
}

VOIDPTR ClientHandler(VOIDPTR arg)
{
    int socketFd = *((INTPTR) arg);
    sem_wait(&clientSem);

    

    sem_post(&clientSem);
    pthread_exit(NULL);
}