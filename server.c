/* SERVER IMPLEMENTATION FILE
 *
 * Created BY: EMRE ARDIÇ
 * Creation DATE : 6/06/2012
 *
 * Description :
 *
 */

#include "server.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h> /* memset */
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

int sharedMemoryGlobal;

/* Code to establish a socket; originally from bzs@bu-cs.bu.edu */
int CreateSocket(unsigned short portNum, int maxConnection) {
    char myname[MAXHOSTNAME + 1];
    int socketFd;
    struct sockaddr_in sa;
    struct hostent *hp;

    memset(&sa, 0, sizeof (struct sockaddr_in));

    gethostname(myname, MAXHOSTNAME);

    hp = gethostbyname(myname);

    if (hp == NULL) {
        return (FAIL);
    }

    sa.sin_family = hp->h_addrtype;
    sa.sin_port = htons(portNum);

    if ((socketFd = socket(AF_INET, SOCK_STREAM, 0)) == FAIL) {
        perror("Failed to create socket");
        return (FAIL);
    }


    if (bind(socketFd, (struct sockaddr *) &sa, sizeof (struct sockaddr_in)) == FAIL) {
        perror("Failed to bind");
        close(socketFd);
        return (FAIL);
    }

    listen(socketFd, maxConnection);

    return (socketFd);
}

int GetConnection(int socketFd) {
    int retVal;

    if ((retVal = accept(socketFd, NULL, NULL)) == FAIL) {
        perror("Failed to accept");
        return FAIL;
    }

    return retVal;
}

int ReceiveMessage(int socket, VOIDPTR message, int size) {
    LPTSTR data = (LPTSTR) message;
    int i,
            ret;

    for (i = 0; i < size; ++i) {
        if ((ret = read(socket, data + i, 1)) == FAIL) {
            perror("Failed to get data from socket");
            return FAIL;
        }
    }

    return SUCCESS;
}

int SendMessage(int socket, VOIDPTR message, int size) {
    int i,
            ret;

    LPTSTR data = (LPTSTR) message;

    for (i = 0; i < size; ++i) {
        if ((ret = write(socket, data + i, 1)) == FAIL) {
            perror("Failed to send data to socket");
            return FAIL;
        }
    }

    return SUCCESS;
}

void StartServer(int portBegin, const int maxClient, int sharedMemory) {
    int i;
    int sockets[maxClient];
    pthread_t handlers[maxClient];

    // Shared memory'den verileri almak ve clientlere gondermek icin ClientDataHandler'a lazim
    sharedMemoryGlobal = sharedMemory;

    // Her port icin soket ac ve soketleri kaydet
    for (i = 0; i < maxClient; ++i) {
        sockets[i] = CreateSocket(portBegin + i, MAXCONNECTION);
        // Her thread bir portu dinlicek, acilan soket thread'lere veriliyor
        if (pthread_create(&handlers[i], NULL, ClientConnectionHandler, &sockets[i])) {
            fprintf(stderr, "\nError: Failed to create connection handler\n");
            return;
        }
    }

    // thread'leri bekleyelim
    for (i = 0; i < maxClient; ++i) {
        pthread_join(handlers[i], NULL);
    }

}

VOIDPTR ClientDataHandler(VOIDPTR arg) {
    INTPTR socketFd = (INTPTR) arg;
    char data[20] = "Test datasi";
    SendMessage(*socketFd, data, 20);
    fprintf(stderr, "Data sent\n");
    close(*socketFd);
    // oldur kendini, cunku bu threadi bekleyen yok
    pthread_detach(pthread_self());
    pthread_exit(NULL);
}

VOIDPTR ClientConnectionHandler(VOIDPTR arg) {

    int socketFd, i;
    INTPTR socket = (INTPTR) arg;
    pthread_t dataHandler;

    /* The loop which handles incoming connections */
    i = 0;
    for (;;) {
        if ((socketFd = GetConnection(*socket)) == FAIL) {
            fprintf(stderr, "\nError:Failed to accept connection.\n");
            return;
        }
        // Bu porta birden fazla client baglanabilir, her biri icin bir thread
        // Gelen baglantinin fd'si thread'e veriliyor haberlesme icin
        if (pthread_create(&dataHandler, NULL, ClientDataHandler, &socketFd)) {
            fprintf(stderr, "\nError: Failed to create data handler\n");
            return;
        }
        //MAXCONNECTION olunca dizinin basina donecek, kotu cozum ama calisir
        i = (i + 1) % MAXCONNECTION;
    }/* End of for */

    close(*socket);
    pthread_exit(NULL);
}


