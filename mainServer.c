/* SERVER DRIVER
 *
 * Created BY: EMRE ARDIÇ
 * Creation DATE : 25/05/2012
 *
 * Description :
 *
 */

#include "server.h"
#include "types.h"
#include <stdlib.h>
#include <string.h>

void GetArguments(int argc, LPTSTR argv[], INTPTR shared, INTPTR maxClient, INTPTR portBegin);

void Usage(LPTSTR progName);

int main(int argc, LPTSTR argv[]) {
    int portBegin, maxClient, sharedMemory;
    GetArguments(argc, argv, &sharedMemory, &maxClient, &portBegin);
    StartServer(portBegin, maxClient, sharedMemory);
    return EXIT_SUCCESS;
}

/* Returns tree_depth */
void GetArguments(int argc, LPTSTR argv[], INTPTR shared, INTPTR maxClient, INTPTR portBegin) {
    if (argc < 2) {
        fprintf(stderr, "\nError :Missing argument.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

    if (!strcmp(argv[1], "-h")) {
        Usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    if (argv[1] != NULL && !strcmp(argv[1], "-sharedMemory")) {
        if (argv[2] == NULL) {
            fprintf(stderr, "\nError :Invalid shared memory.\nTry -h.\n\n");
            exit(EXIT_FAILURE);
        }
        *shared = atoi(argv[2]);
    } else {
        fprintf(stderr, "\nError :Shared memory option error.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

    if (argv[3] != NULL && !strcmp(argv[3], "-maxClient")) {
        if (argv[4] == NULL) {
            fprintf(stderr, "\nError :Invalid max client.\nTry -h.\n\n");
            exit(EXIT_FAILURE);
        }
        *maxClient = atoi(argv[4]);
    } else {
        fprintf(stderr, "\nError :Client num option error.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

    if (argv[5] != NULL && !strcmp(argv[5], "-portBegin")) {
        if (argv[6] == NULL) {
            fprintf(stderr, "\nError :Invalid port.\nTry -h.\n\n");
            exit(EXIT_FAILURE);
        }
        *portBegin = atoi(argv[6]);
    } else {
        fprintf(stderr, "\nError :Port begin option error.\nTry -h.\n\n");
        exit(EXIT_FAILURE);
    }

}

/* Prints usage to screen */
void Usage(LPTSTR progName) {
    printf("\nThe Server program sending function data to filter clients.");
    printf("\nOption : -h for usage.");
    printf("\nUsage : %s -sharedMemory [#] -maxClient [#] -portBegin [#]\n\n", progName);
}
