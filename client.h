/* CLIENT HEADER
 *
 * Created BY: EMRE ARDIÇ
 * Creation DATE : 25/05/2012
 *
 * Description :
 *
 * Prototypes of client functions.
 * FAIL,SUCCESS defines.
 *
 */

#ifndef CILENT_H
#define CLIENT_H

#include "types.h"

#define SUCCESS 0
#define FAIL -1
#define FALSE 0
#define TRUE 1

#define BILLION 1000000000L
#define MAXHOSTNAME 30

typedef struct {
    char client[MAXHOSTNAME],
    server[MAXHOSTNAME];

    unsigned short port;

    double cutOff, dampingRatio;

} SClient;
typedef SClient * CLIENTPTR;

int ReceiveMessage(int socket, VOIDPTR message, int size);
int SendMessage(int socket, VOIDPTR message, int size);
double GetNanoSecondTime();
int StartClient(double cutoff, double damping, LPTSTR server, int port);
int ConnectServer(LPTSTR hostname, unsigned short portNum);

#endif
